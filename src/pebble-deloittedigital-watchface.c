#include "pebble_os.h"
#include "pebble_app.h"
#include "pebble_fonts.h"


#define MY_UUID { 0x44, 0x7A, 0x2E, 0xE9, 0x31, 0xCF, 0x42, 0x24, 0x80, 0x1D, 0xB0, 0xBE, 0x48, 0xAF, 0x2C, 0xD3 }
PBL_APP_INFO(MY_UUID,
             "Deloitte Digital", "Vlado Grancaric",
             1, 0, /* App version */
             DEFAULT_MENU_ICON,
             APP_INFO_WATCH_FACE);

#define COLOR_FOREGROUND GColorWhite
#define COLOR_BACKGROUND GColorBlack

Window window;

TextLayer text_time_layer;
TextLayer deloitte_text;
TextLayer digital_text;
Layer deloitte_dot;

void init_time_layer(TextLayer* textlayer, int x, int y, int width, int height)
{
	text_layer_init(textlayer, GRect(x, y, width, height));
	text_layer_set_text_color(textlayer, COLOR_FOREGROUND);
	text_layer_set_background_color(textlayer, GColorClear);
	text_layer_set_font(textlayer, fonts_get_system_font(FONT_KEY_GOTHAM_42_BOLD));
}

// utility function for initializing a text layer
void init_text(TextLayer* textlayer, int x, int y, int width, int height, ResourceId font)
{
	text_layer_init(textlayer, GRect(x, y, width, height));
	text_layer_set_text_alignment(textlayer, GTextAlignmentLeft);
	text_layer_set_text_color(textlayer, COLOR_FOREGROUND);
	text_layer_set_background_color(textlayer, GColorClear);
	//text_layer_set_font(textlayer, fonts_get_system_font(FONT_KEY_GOTHAM_42_BOLD));	
	text_layer_set_font(textlayer, fonts_load_custom_font(resource_get_handle(font)));
}

// callback to draw the dot
void draw_dot_callback(Layer *me, GContext* ctx)
{
	GRect layerFrame = layer_get_frame(me);
		
	(void) me;
	
	graphics_context_set_fill_color(ctx, COLOR_FOREGROUND);	
	graphics_fill_circle(ctx, GPoint(layerFrame.size.w/2,layerFrame.size.h/2), layerFrame.size.w/2);
	graphics_draw_circle(ctx, GPoint(layerFrame.size.w/2,layerFrame.size.h/2), layerFrame.size.w/2);		
}

void handle_init(AppContextRef ctx) {
	(void)ctx;

	window_init(&window, "Deloitte Digital Clock"); 
	window_stack_push(&window, true /* Animated */);
	window_set_background_color(&window, COLOR_BACKGROUND);	
  
  	//Init the current resources
	resource_init_current_app(&APP_RESOURCES);
	  	  
	//Init Text Layers
	init_time_layer(&text_time_layer,2,2,100,45);	
	init_text(&deloitte_text, 2, 48, 118, 68, RESOURCE_ID_FONT_ROBOTO_BOLD_SUBSET_32);	
	init_text(&digital_text, 2, 86, 100, 68, RESOURCE_ID_FONT_ROBOTO_BOLD_SUBSET_32);
	
	//Init the Dot layer
	int dotX = (1 + deloitte_text.layer.frame.size.w);
	
	layer_init(&deloitte_dot, GRect(dotX, 70, 10, 10));		
	deloitte_dot.update_proc = &draw_dot_callback;	
	
	//Set the text for the initial text layers
	text_layer_set_text(&deloitte_text, "Deloitte");
	text_layer_set_text(&digital_text, "Digital");	
	
	//Add the layers to the main window layer
	layer_add_child(&window.layer, &text_time_layer.layer);		
	layer_add_child(&window.layer, &deloitte_text.layer);
	layer_add_child(&window.layer, &digital_text.layer);
	layer_add_child(&window.layer, &deloitte_dot);	
}

void handle_minute_tick(AppContextRef ctx, PebbleTickEvent *t) {

  (void)ctx;

  // Need to be static because they're used by the system later.
  static char time_text[] = "00:00";

  char *time_format;

  if (clock_is_24h_style()) {
    time_format = "%R";
  } else {
    time_format = "%I:%M";
  }

  string_format_time(time_text, sizeof(time_text), time_format, t->tick_time);

  // Kludge to handle lack of non-padded hour format string
  // for twelve hour clock.
  if (!clock_is_24h_style() && (time_text[0] == '0')) {
    memmove(time_text, &time_text[1], sizeof(time_text) - 1);
  }

  text_layer_set_text(&text_time_layer, time_text);

}


void pbl_main(void *params) {
  PebbleAppHandlers handlers = {
    .init_handler = &handle_init,
	
	.tick_info = {
	      .tick_handler = &handle_minute_tick,
	      .tick_units = MINUTE_UNIT
	}
		
  };
  app_event_loop(params, &handlers);
}
